#include "./test/test.c"
#include "./structs/structs.c"
#include <time.h>

int main () {

    srand(time(0));

    populacja *wioska = NULL;
    wioska = inicjalizacja_populacji_losowej(20);

    osobnik *dziecko = NULL;
    dziecko = test_rodzenia(wioska);

    return EXIT_SUCCESS;
}

