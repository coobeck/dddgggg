#include "./structs.c"

#ifndef METHODS_C
#define METHODS_C

osobnik *rodzenie(osobnik *mama, osobnik *tata) {

    osobnik *dziecko = malloc(sizeof(osobnik));

    size_t dlugosc_mama = mama->dlugosc_chromosomu;
    size_t dlugosc_tata = tata->dlugosc_chromosomu;

    size_t przeciecie_mama = (rand() % dlugosc_mama);
    size_t przeciecie_tata = (rand() % dlugosc_tata);
    //zagwarantowanie, ze dlugosc jest większa od 0
    ++przeciecie_mama;
    ++przeciecie_tata;

    dziecko->dlugosc_chromosomu = przeciecie_tata + dlugosc_mama - przeciecie_mama + 1;
    dziecko->chromosom = calloc(dziecko->dlugosc_chromosomu, sizeof(int));

        for (int i = 0; i < przeciecie_tata; ++i) {
            dziecko->chromosom[i] = tata->chromosom[i];
        }
        int j = 0;
        for (int i = przeciecie_tata; i < dziecko->dlugosc_chromosomu; ++i) {
            dziecko->chromosom[i] = mama->chromosom[przeciecie_mama + j++ - 1];
        }
    return dziecko;
}

void dopasowanie(osobnik * dopasowywany) {
    float wartosc = 1;
    int parametr;
    for (int i = 0; i < dopasowywany->dlugosc_chromosomu; ++i) {
        parametr = dopasowywany->chromosom[i];
        if (parametr % 3) wartosc *= 0.97;
        if (parametr % 2) wartosc *= 0.95;
        wartosc *= 0.99;
    }
    dopasowywany->dopasowanie = wartosc;
}

#endif
