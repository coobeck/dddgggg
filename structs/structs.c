#include <stdlib.h>

#ifndef STRUCTS_C
#define STRUCTS_C

typedef struct osobnik {
    int *chromosom;
    size_t dlugosc_chromosomu;
    float dopasowanie;
} osobnik;

typedef struct populacja {
    size_t liczba_osobnikow;
    osobnik **lista_osobnikow;
    osobnik *martwe;
    osobnik *zywe;
    osobnik *rozmnazanie;
} populacja;

#endif
