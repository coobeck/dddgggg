#include <stdio.h>
#include "../structs//structs.c"
#include "../structs/methods.c"

#define MAX_CHROMO_RODZICA 10
#define MAX_WARTOSC_GENU 20

/* ROBIENIE TATY I MAMY XDDDD */

populacja *inicjalizacja_populacji_losowej(size_t ilosc) {

    populacja * dwaplusjeden = malloc(sizeof(populacja));

    dwaplusjeden->liczba_osobnikow = ilosc;
    dwaplusjeden->lista_osobnikow = calloc(ilosc, sizeof(osobnik *));

    size_t dlugosc_chromosomu;

    for (size_t i = 0; i < ilosc; ++i) {

        dlugosc_chromosomu = rand() % MAX_CHROMO_RODZICA + 1;
        dwaplusjeden->lista_osobnikow[i] = malloc(sizeof(osobnik));
        dwaplusjeden->lista_osobnikow[i]->dlugosc_chromosomu = dlugosc_chromosomu;
        dwaplusjeden->lista_osobnikow[i]->chromosom = calloc(dwaplusjeden->lista_osobnikow[i]->dlugosc_chromosomu, sizeof(int));
        printf("Chromosom osobnika nr : %ld\n", i);
        for (int j = 0; j < dlugosc_chromosomu; ++j) { //generujemy losowe wartosci w chromosomie
            dwaplusjeden->lista_osobnikow[i]->chromosom[j] = rand() % MAX_WARTOSC_GENU + 1;
            printf("%d ", (dwaplusjeden->lista_osobnikow[i])->chromosom[j]);
        }
        printf("\n");
    }
    return dwaplusjeden;
}

osobnik * test_rodzenia (populacja *wioska) {
    size_t indekstaty = rand() % wioska->liczba_osobnikow;
    size_t indeksmamy = indekstaty;
    do {
        indeksmamy = rand() % wioska->liczba_osobnikow;
    } while (indeksmamy == indekstaty);

    osobnik *tata = wioska->lista_osobnikow[indekstaty];
    osobnik *mama = wioska->lista_osobnikow[indeksmamy];

    osobnik *dziecko = NULL;

    dziecko = rodzenie(mama, tata);

    printf("\nChromosom dziecka rodziców o indeksach %ld i %ld:\n", indekstaty, indeksmamy);
    for (int j = 0; j < dziecko->dlugosc_chromosomu; ++j) {
        printf("%d ", dziecko->chromosom[j]);
    }
    printf("\n");
    return dziecko;
}
